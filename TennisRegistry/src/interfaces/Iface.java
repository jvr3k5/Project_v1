package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import Model.PlayerMapperUtils;
import Model.TournamentMapperUtils;
import server.Player;
import server.Tournament;



public interface Iface extends Remote {
	
	
	void showPlayers() throws RemoteException;
	void showTournament() throws RemoteException;
	void matchResult() throws RemoteException;

}
