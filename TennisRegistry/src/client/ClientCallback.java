package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import Model.PlayerMapperUtils;
import interfaces.ICallback;
import interfaces.Iface;

public class ClientCallback extends UnicastRemoteObject
implements ICallback{

	public ClientCallback() throws RemoteException {
		super();

	}

	@Override
	public void showPlayers() throws RemoteException {
		System.out.println(PlayerMapperUtils.readPlayersFromFile());
		
	}

	@Override
	public void showTournamentt(Iface iface) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
}
