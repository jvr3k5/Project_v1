package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import interfaces.ICallback;
import interfaces.Iface;

public class Client {

	static Iface remoteObject;
	static Scanner userInput = new Scanner(System.in);
	
	
	public static void main(String[] args) {
		while(true) {
			System.out.println("Welcome to tennis registry. Please input one of the following:"
					+ "\n1 - Players info"
					+ "\n2 - Tournament info"
					+ "\n3 - Results");
			new Client();
		}
	}
	
	public Client() {
		String inputLine = userInput.nextLine();
		Registry registry;
		int intInputLine = Integer.valueOf(inputLine);
			
		try {	
			registry = LocateRegistry.getRegistry();
			remoteObject = (Iface) registry.lookup("Server");
			
			
		switch(intInputLine ) {
			case 1: remoteObject.showPlayers();
				break;
			case 2: remoteObject.showTournament();
				break;
			case 3: remoteObject.matchResult();
				break;
			default: System.out.println("Incorrect number");
		
		}
			} catch (RemoteException e) {
					e.printStackTrace();
				} catch (NotBoundException e) {
				e.printStackTrace();
			}
	}
}


