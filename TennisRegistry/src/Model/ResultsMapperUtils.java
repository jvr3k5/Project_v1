package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class ResultsMapperUtils {
	
	private static final String RESULTS_FILENAME = "C:\\Users\\jarek\\Desktop\\Projekt_InfWMech\\TennisRegistry\\results.txt";
	
	public ResultsMapperUtils() {}
	
	public static List<String> readResultsFromFile() {
		File file = new File(RESULTS_FILENAME);		
		List<String> resultsDataAsList = new ArrayList<>();
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			while(sc.hasNextLine()) {
				String temp = sc.nextLine();
				resultsDataAsList.add(temp);
				}

	} catch(FileNotFoundException ex) {
		System.out.println("File could not be read. Invalid path: " + RESULTS_FILENAME);
	} finally {
		sc.close();
	}
		return resultsDataAsList;
	}
}