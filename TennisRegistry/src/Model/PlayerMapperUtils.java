package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import server.Player;


public class PlayerMapperUtils implements Serializable {

	private static final String PLAYERS_FILENAME = "C:\\Users\\jarek\\Desktop\\Projekt_InfWMech\\TennisRegistry\\player.txt";
		
	public PlayerMapperUtils() {}
	
	public static List<Player> readPlayersFromFile() {
		File file = new File(PLAYERS_FILENAME);		
		List<Player> playersDataAsList = new ArrayList<>();
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			while(sc.hasNextLine()) {
				String temp = sc.nextLine();
				String[] playerData = temp.split(";");
				Player player = getPlayerOrNull(playerData);
				if (player != null) {
					playersDataAsList.add(player);
				}
			}
		} catch(FileNotFoundException ex) {
			System.out.println("File could not be read. Invalid path: " + PLAYERS_FILENAME);
		} finally {
			sc.close();
		}
		
		return playersDataAsList;
	}
	private static Player getPlayerOrNull(String[] playerData) {
		return Player.createPlayer(playerData);	
	}
}