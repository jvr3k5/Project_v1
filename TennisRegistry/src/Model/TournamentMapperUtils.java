package Model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import server.Tournament;


public class TournamentMapperUtils {

//	public static void main(String[] args) {
		
		private static final String TOURNAMENT_FILENAME = "C:\\Users\\jarek\\Desktop\\Projekt_InfWMech\\TennisRegistry\\tournament.txt";
	
		public static List<Tournament> readTournamentFromFile() {
		List<Tournament> tournamentDataAsList = new ArrayList<>(); 
		
		File file = new File(TOURNAMENT_FILENAME);
		
		try {
			Scanner sc = new Scanner(file);
			
			while(sc.hasNextLine()) {
				
				String temp = sc.nextLine();
				String[] tournamentData= temp.split(";");
				
				Tournament tournamnet = Tournament.createTournament(tournamentData);
				if(tournamnet != null) {
					tournamentDataAsList.add(tournamnet);
				}

			}
			sc.close();
			
		} catch (Exception e) {
			System.out.println("File could not be read");
		}
		return tournamentDataAsList;
		}
}
