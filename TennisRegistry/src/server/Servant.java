package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import Model.PlayerMapperUtils;
import Model.TournamentMapperUtils;
import interfaces.Iface;


public class Servant extends UnicastRemoteObject implements Iface {

	protected Servant() throws RemoteException{}
	
	@Override
	public void matchResult() throws RemoteException {
		Result.getPlayers();
	}


	@Override
	public void showPlayers() throws RemoteException {	
		System.out.println(PlayerMapperUtils.readPlayersFromFile());
		
	}

	@Override
	public void showTournament() throws RemoteException {
		System.out.println(TournamentMapperUtils.readTournamentFromFile());
		
	}
}