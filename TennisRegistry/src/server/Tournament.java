package server;



public class Tournament {

	String tournament;
	String venue;
	String court_type;
	String champs_men;
	String champs_women;


public Tournament(String tournament_temp, String venue_temp, String court_type_temp, String champs_men_temp, String champs_women_temp) {
	tournament = tournament_temp;
	venue = venue_temp;
	court_type = court_type_temp;
	champs_men = champs_men_temp;
	champs_women = champs_women_temp;

	}

private static boolean tournamentCheck(String[] tournamentData) {
	
	if(tournamentData.length != 5) return false;
	String tournament = tournamentData[0];
	String venue = tournamentData[1];
	String court_type = tournamentData[2];
	String champs_men = tournamentData[3];
	String champs_women = tournamentData[4];
	
	
	
	if(!tournament.matches("US Open|Australian Open|Wimbledon|French Open"))return false;
	if(!venue.matches("^(([\\D])[\\D]+)$")) return false;
	if(!court_type.matches("Clay|Hardcourt|Grass")) return false;
	if(!champs_men.matches("^([\\D][\\-]?)(\\D)((\\/)?)(\\D)+$")) return false;
	if(!champs_women.matches("^([\\D][\\-]?)(\\D)((\\/)?)(\\D)+$")) return false;
	
	else return true;
	
	
	}

public static Tournament createTournament(String[] tournamentData) {
	if(tournamentCheck(tournamentData)) {
		return new Tournament(tournamentData[0], tournamentData[1], tournamentData[2], tournamentData[3], tournamentData[4]);
	}
	return null;
}

@Override
public String toString() {
	return "Tournament: " + tournament + " venue = " + venue +  ", court_type = " + court_type + ", champs_men(single-double) = "
			+ champs_men + ", champs_women(single-double) = " + champs_women + "";
}


}