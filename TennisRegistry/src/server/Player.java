package server;

import java.io.Serializable;

public class Player implements Serializable{

	int id;
	String name;
	String surname;
	String age;
	String hand;
	
	
	public Player(String name_temp, String surname_temp, String age_temp, String hand_temp) {
		name = name_temp;
		surname = surname_temp;
		age = age_temp;
		hand = hand_temp;
	}


	private static boolean validatePlayer(String[] playerData) {
		
		if(playerData.length != 5) return false;
		
		String name = playerData[1];
		String surname = playerData[2];
		String age = playerData[3];
		String hand = playerData[4];
		
		if((name.length() < 2 || name.length() > 20) ||
		(surname.length() < 2 || surname.length() > 20) ||
		(Integer.valueOf(age) < 20 || Integer.valueOf(age) > 40) ||
		(!hand.equals("Left") && !hand.equals("Right"))) return false;
		
		else return true;
	}

	
	public static Player createPlayer(String[] playerData) {
		//validating each parameter - name, surname...
		if(validatePlayer(playerData)) {
			return new Player(playerData[1], playerData[2], playerData[3], playerData[4]);
		}
		return null;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public String getHand() {
		return hand;
	}


	public void setHand(String hand) {
		this.hand = hand;
	}


	@Override
	public String toString() {
		return "Player: name = " + name + ", surname = " + surname + ", age = " + age + ", hand = " + hand + "";
	}
	
}	
