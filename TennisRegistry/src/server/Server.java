package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import Model.PlayerMapperUtils;
import Model.TournamentMapperUtils;
import client.ClientCallback;
import interfaces.ICallback;
import interfaces.Iface;


public class Server {

	Registry registry;
	Servant servant;	

	public static void main(String[] args) {
		try {
			new Server();
		} catch(Exception ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}
	
	protected Server() throws RemoteException{
//		init();
		try {
			registry = LocateRegistry.createRegistry(1099);
			servant = new Servant();
			System.setProperty("java.rmi.server.hostname","192.168.1.3");
			registry.rebind("Server", servant);
			System.out.println("Server READY");
		} catch(RemoteException ex){
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public void updateScore() {
		
	}
	
//	public void init() {
//		DataListStorage.playersList = PlayerMapperUtils.readPlayersFromFile();
//		DataListStorage.tournamentsList = TournamentMapperUtils.readTournamentFromFile();

//	}
}

